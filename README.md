A demo application to introduce WebAPI + AngularJS to my development team

TODO: add edit/delete funtionality to the front end

REQUIREMENT: VS STUDIO >= 2012, SQL SERVER

INSTALL:

    *Sign up for api key on ROTTEN TOMATOES,then go to MovieTheaterRating.Data -> Configurations -> DatabaseInitializer.cs, replace TOMATO_URL with your api key from ROTTEN TOMATOES
    *Open the solution using visual studio, change connection string in each project to match yours.
    *If you have problems with nuget, delete the packages folder from solution and also bin and obj folders from every project in the solution and give it a rebuild
    *Set Console application as a start up project, then run the app to initialize the database with seed data
    *Right click on solution folder -> Properties -> start multiple project, choose MovieTheaterRating.Client and MovieTheaterRating.WebApi.
    *Build and run the app